package ru.t1.akolobov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
public class UserProfileResponse extends AbstractUserResponse {

    public UserProfileResponse(@Nullable User user) {
        super(user);
    }

}
