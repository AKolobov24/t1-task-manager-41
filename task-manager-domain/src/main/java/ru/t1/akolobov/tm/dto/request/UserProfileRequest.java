package ru.t1.akolobov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class UserProfileRequest extends AbstractUserRequest {

    public UserProfileRequest(@Nullable String token) {
        super(token);
    }

}
