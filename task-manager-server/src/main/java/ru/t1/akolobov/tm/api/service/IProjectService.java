package ru.t1.akolobov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.model.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    @NotNull Project add(@Nullable String userId, @Nullable Project project);

    @NotNull Project update(@Nullable String userId, @Nullable Project project);

    void clear(@Nullable String userId);

    void clear();

    boolean existById(@Nullable String userId, @Nullable String id);

    @NotNull List<Project> findAll();

    @NotNull List<Project> findAll(@Nullable String userId);

    @NotNull List<Project> findAll(@Nullable String userId, @Nullable Sort sort);

    @Nullable Project findOneById(@Nullable String userId, @Nullable String id);

    @NotNull Integer getSize(@Nullable String userId);

    @NotNull Project remove(@Nullable String userId, @Nullable Project project);

    void removeById(@Nullable String userId, @Nullable String id);

    @NotNull
    Project changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @Nullable
    Project create(@Nullable String userId, @Nullable String name);

    @Nullable
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    Project create(@Nullable String userId, @Nullable String name, @Nullable Status status);

    @NotNull
    Project updateById(@Nullable String userId,
                       @Nullable String id,
                       @Nullable String name,
                       @Nullable String description);

    @NotNull Collection<Project> add(@NotNull Collection<Project> models);

    @NotNull Collection<Project> set(@NotNull Collection<Project> models);

}
