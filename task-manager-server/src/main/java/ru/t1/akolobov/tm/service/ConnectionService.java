package ru.t1.akolobov.tm.service;

import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.repository.IProjectRepository;
import ru.t1.akolobov.tm.api.repository.ISessionRepository;
import ru.t1.akolobov.tm.api.repository.ITaskRepository;
import ru.t1.akolobov.tm.api.repository.IUserRepository;
import ru.t1.akolobov.tm.api.service.IConnectionService;
import ru.t1.akolobov.tm.api.service.IDatabaseProperty;
import ru.t1.akolobov.tm.dto.model.ProjectDTO;
import ru.t1.akolobov.tm.dto.model.SessionDTO;
import ru.t1.akolobov.tm.dto.model.TaskDTO;
import ru.t1.akolobov.tm.dto.model.UserDTO;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

public final class ConnectionService implements IConnectionService {

    @NotNull
    final SqlSessionFactory sqlSessionFactory;

    @NotNull
    final EntityManagerFactory entityManagerFactory;

    @NotNull
    private final IDatabaseProperty databaseProperty;

    public ConnectionService(@NotNull final IDatabaseProperty databaseProperty) {
        this.databaseProperty = databaseProperty;
        this.sqlSessionFactory = getSqlSessionFactory();
        this.entityManagerFactory = factory();
    }

    @Override
    @NotNull
    public EntityManagerFactory factory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(org.hibernate.cfg.Environment.DRIVER, databaseProperty.getDatabaseDriver());
        settings.put(org.hibernate.cfg.Environment.URL, databaseProperty.getDatabaseUrl());
        settings.put(org.hibernate.cfg.Environment.USER, databaseProperty.getDatabaseUser());
        settings.put(org.hibernate.cfg.Environment.PASS, databaseProperty.getDatabasePassword());
        settings.put(org.hibernate.cfg.Environment.DIALECT, databaseProperty.getDatabaseDialect());
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, databaseProperty.getDatabaseDDLAuto());
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, databaseProperty.getDatabaseShowSQL());
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources source = new MetadataSources(registry);
        source.addAnnotatedClass(UserDTO.class);
        source.addAnnotatedClass(SessionDTO.class);
        source.addAnnotatedClass(ProjectDTO.class);
        source.addAnnotatedClass(TaskDTO.class);
        @NotNull final Metadata metadata = source.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @NotNull
    @Override
    public SqlSessionFactory getSqlSessionFactory() {
        @NotNull final DataSource dataSource = new PooledDataSource(
                databaseProperty.getDatabaseDriver(),
                databaseProperty.getDatabaseUrl(),
                databaseProperty.getDatabaseUser(),
                databaseProperty.getDatabasePassword()
        );
        @NotNull final TransactionFactory transactionFactory =
                new JdbcTransactionFactory();
        @NotNull final Environment environment =
                new Environment("development", transactionFactory, dataSource);
        @NotNull final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(ISessionRepository.class);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

    @NotNull
    @Override
    public SqlSession getConnection() {
        return getConnection(false);
    }

    @NotNull
    @Override
    public SqlSession getConnection(boolean autoCommit) {
        return sqlSessionFactory.openSession(autoCommit);
    }

}
