package ru.t1.akolobov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.enumerated.Role;
import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.model.User;

import java.util.Collection;
import java.util.List;

public interface IUserService {

    @NotNull User add(@Nullable User user);

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    @NotNull
    User removeByLogin(@Nullable String login);

    @NotNull
    User removeByEmail(@Nullable String email);

    @NotNull
    User remove(@Nullable User user);

    int removeById(@Nullable String id);

    @NotNull
    User setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    User updateUser(@Nullable String id,
                    @Nullable String firstName,
                    @Nullable String lastName,
                    @Nullable String middleName);

    void lockUserByLogin(String login);

    void unlockUserByLogin(String login);

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

    void clear();

    boolean existById(@Nullable String id);

    @NotNull
    List<User> findAll(@Nullable Sort sort);

    @NotNull
    List<User> findAll();

    @Nullable
    User findOneById(@Nullable String id);

    @NotNull
    Collection<User> set(@NotNull Collection<User> models);

}
