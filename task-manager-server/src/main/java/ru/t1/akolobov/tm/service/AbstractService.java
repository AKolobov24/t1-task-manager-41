package ru.t1.akolobov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.service.IConnectionService;

public abstract class AbstractService {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
