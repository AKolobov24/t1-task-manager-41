package ru.t1.akolobov.tm.service;

import org.apache.ibatis.session.LocalCacheScope;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.akolobov.tm.api.repository.IProjectRepository;
import ru.t1.akolobov.tm.api.repository.ITaskRepository;
import ru.t1.akolobov.tm.api.repository.IUserRepository;
import ru.t1.akolobov.tm.api.service.IConnectionService;
import ru.t1.akolobov.tm.api.service.IProjectTaskService;
import ru.t1.akolobov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.akolobov.tm.exception.entity.TaskNotFoundException;
import ru.t1.akolobov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.akolobov.tm.exception.field.TaskIdEmptyException;
import ru.t1.akolobov.tm.exception.field.UserIdEmptyException;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.model.Project;
import ru.t1.akolobov.tm.model.Task;

import java.util.List;

import static ru.t1.akolobov.tm.data.TestProject.createProjectList;
import static ru.t1.akolobov.tm.data.TestTask.createTaskList;
import static ru.t1.akolobov.tm.data.TestUser.*;

@Category(UnitCategory.class)
public class ProjectTaskServiceTest {

    @NotNull
    private final static PropertyService propertyService = new PropertyService();
    @NotNull
    private final static IConnectionService connectionService = new ConnectionService(propertyService);
    @NotNull
    private final static SqlSession repositoryConnection = connectionService.getConnection(true);
    @NotNull
    private final static IUserRepository userRepository = repositoryConnection.getMapper(IUserRepository.class);
    @NotNull
    private final ITaskRepository taskRepository = repositoryConnection.getMapper(ITaskRepository.class);
    @NotNull
    private final IProjectRepository projectRepository = repositoryConnection.getMapper(IProjectRepository.class);
    @NotNull
    private final IProjectTaskService service = new ProjectTaskService(connectionService);

    @BeforeClass
    public static void addUsers() {
        repositoryConnection.getConfiguration().setCacheEnabled(false);
        repositoryConnection.getConfiguration().setLocalCacheScope(LocalCacheScope.STATEMENT);
        userRepository.add(USER1);
        userRepository.add(USER2);
    }

    @AfterClass
    public static void clearUsers() {
        userRepository.remove(USER1);
        userRepository.remove(USER2);
        repositoryConnection.close();
    }

    @Before
    public void initRepository() {
        createTaskList(USER1_ID).forEach(taskRepository::add);
        createProjectList(USER1_ID).forEach(projectRepository::add);
    }

    @After
    public void clearRepository() {
        taskRepository.clear(USER1_ID);
        projectRepository.clear(USER1_ID);
        taskRepository.clear(USER2_ID);
        projectRepository.clear(USER2_ID);
    }

    @Test
    public void bindTaskToProject() {
        @NotNull Project project = projectRepository.findAllByUserId(USER1_ID).get(0);
        @NotNull Task task = taskRepository.findAllByUserId(USER1_ID).get(0);
        service.bindTaskToProject(USER1_ID, project.getId(), task.getId());
        Task bindTask = taskRepository.findOneById(USER1_ID, task.getId());
        Assert.assertNotNull(bindTask);
        Assert.assertEquals(project.getId(), bindTask.getProjectId());

        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.bindTaskToProject(USER_EMPTY_ID, project.getId(), task.getId())
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> service.bindTaskToProject(USER1_ID, USER_EMPTY_ID, task.getId())
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> service.bindTaskToProject(USER1_ID, project.getId(), USER_EMPTY_ID)
        );
        Assert.assertThrows(
                TaskNotFoundException.class,
                () -> service.bindTaskToProject(USER1_ID, project.getId(), project.getId())
        );
        Assert.assertThrows(
                ProjectNotFoundException.class,
                () -> service.bindTaskToProject(USER1_ID, task.getId(), project.getId())
        );
    }

    @Test
    public void removeProjectById() {
        @NotNull Project project = projectRepository.findAllByUserId(USER1_ID).get(0);
        @NotNull List<Task> taskList = taskRepository.findAllByUserId(USER1_ID);
        service.bindTaskToProject(USER1_ID, project.getId(), taskList.get(0).getId());
        service.bindTaskToProject(USER1_ID, project.getId(), taskList.get(1).getId());

        service.removeProjectById(USER1_ID, project.getId());
        Assert.assertEquals(taskList.size() - 2, taskRepository.findAllByUserId(USER1_ID).size());
        Assert.assertFalse(taskRepository.findAllByUserId(USER1_ID).contains(taskList.get(0)));
        Assert.assertFalse(taskRepository.findAllByUserId(USER1_ID).contains(taskList.get(1)));

        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.removeProjectById(USER_EMPTY_ID, project.getId())
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> service.removeProjectById(USER1_ID, USER_EMPTY_ID)
        );
    }

    @Test
    public void unbindTaskFromProject() {
        @NotNull Project project = projectRepository.findAllByUserId(USER1_ID).get(0);
        @NotNull Task task = taskRepository.findAllByUserId(USER1_ID).get(0);
        service.bindTaskToProject(USER1_ID, project.getId(), task.getId());
        Task bindTask = taskRepository.findOneById(USER1_ID, task.getId());
        Assert.assertNotNull(bindTask);
        Assert.assertEquals(project.getId(), bindTask.getProjectId());

        service.unbindTaskFromProject(USER1_ID, task.getId());
        bindTask = taskRepository.findOneById(USER1_ID, task.getId());
        Assert.assertNotNull(bindTask);
        Assert.assertNotEquals(project.getId(), bindTask.getProjectId());
        Assert.assertNull(bindTask.getProjectId());

        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.unbindTaskFromProject(USER_EMPTY_ID, task.getId())
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> service.unbindTaskFromProject(USER1_ID, USER_EMPTY_ID)
        );
        Assert.assertThrows(
                TaskNotFoundException.class,
                () -> service.unbindTaskFromProject(USER1_ID, project.getId())
        );
    }

}
